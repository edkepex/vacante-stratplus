package stratplus.clients.soap;

import org.tempuri.CalculatorSoap;

import io.quarkiverse.cxf.annotation.CXFClient;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class Calculator {

    @Inject
    @CXFClient("edcalculator")
    CalculatorSoap calculatorService;

    public int sum(int num1, int num2){
        return calculatorService.add(num1, num2);
    }

    public int divide(int num1, int num2){
        return calculatorService.divide(num1, num2);
    }

    public int multiply(int num1, int num2){
        return calculatorService.multiply(num1, num2);
    }
}
