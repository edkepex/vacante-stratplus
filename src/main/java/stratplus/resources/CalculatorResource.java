package stratplus.resources;

import java.util.Map;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import stratplus.clients.soap.Calculator;

@Path("/calculator")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CalculatorResource {

    @Inject
    Calculator calculator;

    @GET
    @Path("/add/{num1}/{num2}")
    public Response add(@PathParam(value = "num1") int num1, @PathParam(value = "num2") int num2) {
        return Response.ok(Map.of("result", calculator.sum(num1, num2))).build();
    }

    @GET
    @Path("/divide/{num1}/{num2}")
    public Response divide(@PathParam(value = "num1") int num1, @PathParam(value = "num2") int num2) {
        return Response.ok(Map.of("result", calculator.divide(num1, num2))).build();
    }

    @GET
    @Path("/multiply/{num1}/{num2}")
    public Response multiply(@PathParam(value = "num1") int num1, @PathParam(value = "num2") int num2) {
        return Response.ok(Map.of("result", calculator.multiply(num1, num2))).build();
    }
}
